package com.lexagle.exer.exer011

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Exer011Application

fun main(args: Array<String>) {
	runApplication<Exer011Application>(*args)
}
